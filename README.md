This is a Client code challenge.

## Getting Started

First, install the required depenencies :

```bash
npm install
# or
yarn
# or
yarn install
```

Second, run the development server:

```bash
npm run start
# or
yarn start
```

Open [http://localhost/api:4000](http://localhost/api:4000) with your browser to see the result.

class Result {
  constructor(value, status = "ok") {
    this.status = status;
    this.value = value;
  }

  static ok(value) {
    return new Result(value, "ok");
  }

  static err(message, code = "USER_INPUT", errors = {}) {
    return new Result(new CustomError(message, code, errors), "err");
  }

  isOk() {
    return this.status === "ok" ? true : false;
  }

  isErr() {
    return this.status === "err" ? true : false;
  }
}

class CustomError {
  constructor(message, code, errors) {
    this.message = message;
    this.code = code;
    this.errors = errors;
  }

  toResult() {
    return Result.err(this);
  }

  static notFound(message, errors = {}) {
    return new CustomError(message, "NOT_FOUND", errors);
  }

  static userInput(message, errors = {}) {
    return new CustomError(message, "USER_INPUT", errors);
  }

  static authentication(message, errors = {}) {
    return new CustomError(message, "AUTHENTICATION", errors);
  }

  static technical(message, errors = {}) {
    return new CustomError(message, "TECHNICAL", errors);
  }
}

module.exports = { Result, CustomError };

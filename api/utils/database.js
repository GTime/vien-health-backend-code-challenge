const { MongoClient } = require("mongodb");

const URL = "mongodb://localhost:27017";

/// Connect Database
async function connectDatabase(dbName) {
  const client = new MongoClient(URL);
  await client.connect();

  // console.log("     Connected Database! ");
  return client.db(dbName);
}

module.exports = {
  connectDatabase,
};

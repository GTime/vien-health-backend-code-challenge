const { Result } = require("../../../utils/result");
const UserRepo = require("../repo");
const User = require("../user");

async function loginUser(context, credentials) {
  const { valid, message, errors } = User.validateForLogin(credentials);


  // Report validity
  if (!valid) return Result.err({ message, errors });

  const repo = new UserRepo(context.database);
  const userResult = await repo.findByEmail(credentials.email);

  // Check if user do no exist
  if (userResult.isErr()) {
    return Result.err("Please create an account");
  }

  // Verify password
  if (!(await userResult.value.verifyPassword(credentials.password))) {
    return Result.err("Invalid user credentials");
  }

  // Generate access token
  const token = userResult.value.generateAccessToken();

  return Result.ok({ token });
}

module.exports = loginUser;

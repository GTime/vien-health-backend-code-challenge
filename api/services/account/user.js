const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const { ACCESSTOKEN_SECRET } = require("../../config");

const SALT_ROUND = 10;

class User {
  #password = null;

  constructor(rawUser) {
    this.id = rawUser.id;
    this.name = rawUser.name;
    this.email = rawUser.email;
    this.#password = rawUser.password;
  }

  /**
   * Create new user and attach id
   *
   * @param {*} rawUser {name, email, password}
   * @returns user
   */
  static async create(rawUser) {
    return new User({
      id: uuidv4(),
      name: rawUser.name,
      email: rawUser.email,
      password: await bcrypt.hash(rawUser.password, SALT_ROUND),
    });
  }

  /**
   * Verify user password
   * @param {*} password
   * @returns status
   */
  async verifyPassword(password) {
    return await bcrypt.compare(password, this.#password);
  }

  /**
   * Generate access token for user
   * @returns token
   */
  generateAccessToken() {
    return jwt.sign({ sub: this.id }, ACCESSTOKEN_SECRET + this.id, {
      expiresIn: "2m",
    });
  }

  toBeSaved() {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
      password: this.#password,
    };
  }

  /**
   * Validate user for register
   * @param {*} unvalidatedUser {name, email, password}
   * @returns validity
   */
  static validateForRegister(unvalidatedUser) {
    const errors = {};

    if (!unvalidatedUser || !unvalidatedUser["name"])
      errors["name"] = "name is required";
    if (!unvalidatedUser || !unvalidatedUser["email"])
      errors["email"] = "email is required";
    if (!unvalidatedUser || !unvalidatedUser["password"])
      errors["password"] = "password is required";

    return Object.values(errors).length > 0
      ? { errors, valid: false, message: "Invalid user" }
      : { errors, valid: true, message: "Valid user" };
  }

  /**
   * Validate user for login
   * @param {*} credentials {email, password}
   * @returns validity
   */
  static validateForLogin(credentials) {
    const errors = {};

    if (!credentials || !credentials["email"])
      errors["email"] = "email is required";
    if (!credentials || !credentials["password"])
      errors["password"] = "password is required";

    return Object.values(errors).length > 0
      ? { errors, valid: false, message: "Invalid credentials" }
      : { errors, valid: true, message: "Valid credentials" };
  }
}

module.exports = User;

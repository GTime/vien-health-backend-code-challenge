"use strict";

const express = require("express");
const account = require("./api/services/account/");
const { connectDatabase } = require("./api/utils/database");
const { setContext } = require("./api/middlewares");

const PORT = process.env.NODE_ENV === "production" ? process.env.PORT : 4000;

const app = express();

async function main() {
  const database = await connectDatabase("vienhealth");

  // Attach Middlewares
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // Attach context to request
  app.use(setContext(database));

  // Attach Router
  app.use("/api", account.router);

  app.listen(PORT, () => {
    console.log(`🎄 Server ready at http://127.0.0.1/api:${PORT}`);
  });
}

// Run app
main().catch(console.log);

module.exports = app;
